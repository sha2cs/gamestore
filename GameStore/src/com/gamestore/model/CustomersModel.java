package com.gamestore.model;

/*
 * <h1>Customers<\h1>
 * <p>Clase que contiene los atributos del objecto Customers<\p>
 */
public class CustomersModel {
	
	private int id;
	private String code;
	private String name;
	private String address;
	private String details;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	
	
	
}
