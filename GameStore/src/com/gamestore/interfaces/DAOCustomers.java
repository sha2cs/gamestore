package com.gamestore.interfaces;

import java.util.List;

import com.gamestore.model.CustomersModel;

/*
 * <h1>DAOCustomers</h1>
 * <p>Interface que contiene los metodos princiaples para la clase customers</p>
 */
public interface DAOCustomers {
	
	public void registrar(CustomersModel customer) throws Exception;
	public void modificar(CustomersModel customer) throws Exception;
	public void eliminar(CustomersModel customer) throws Exception;
	public List<CustomersModel> listar(String name) throws Exception;
	public List<CustomersModel> listarAll() throws Exception;

}
